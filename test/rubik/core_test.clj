(ns rubik.core-test
  (:require [clojure.test :refer :all]
            [rubik.core :as sut]))


(deftest full-face-rotation-identity
  (is (= sut/a-cube
        (->> sut/a-cube
             (iterate #(sut/move % :g))
             (take 5)
             last))))

(def solved-cube
  {[:g :o] [:g :o],
   [:y :o] [:y :o],
   [:g :r] [:g :r],
   [:o :w] [:o :w],
   [:o :w :b] [:o :w :b],
   [:r :b] [:r :b],
   [:g :w :o] [:g :w :o],
   [:y :b] [:y :b],
   [:g :y] [:g :y],
   [:r :b :w] [:r :b :w],
   [:y :o :b] [:y :o :b],
   [:g :y :r] [:g :y :r],
   [:g :r :w] [:g :r :w],
   [:o :b] [:o :b],
   [:r :w] [:r :w],
   [:g :o :y] [:g :o :y],
   [:r :y] [:r :y],
   [:g :w] [:g :w],
   [:r :y :b] [:r :y :b],
   [:w :b] [:w :b]})

(deftest simple-solve
  (is (sut/solved?
        (sut/solve
          (-> solved-cube
              (sut/move :g)
              (sut/move :g)) 3))))
