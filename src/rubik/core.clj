(ns rubik.core
  (:require [medley.core :as med])
  (:import (java.util.concurrent Executors ExecutorService)))


(def a-cube
  {
   [:g :r]     [:y :r];[:w :r]        
   [:g :y :r]  [:o :g :w];[:w :r :b ] 
   [:g :y]     [:w :b];[:w :b]        
   [:g :o :y]  [:w :g :r];[:w :b :o]  
   [:g :o]     [:w :r];[:y :r]   ;    
   [:g :w :o]  [:w :r :b];[:g :w :o]  
   [:g :w]     [:w :g];[:g :o]        
   [:g :r :w ] [:w :b :o];[:g :y :r]  
   [:r :w]     [:g :o];[:w :g]        
   [:r :b :w]  [:y :g :o];[:y :b :r]   
   [:r :y :b]  [:y :o :b];[:y :o :b] ;
   [:r :b]     [:y :b];[:y :b]    
   [:r :y]     [:o :b];[:o :b]        
   [:w :b]     [:w :o];[:b :r]        
   [:o :w]     [:r :b];[:o :w]        
   [:o :w :b]  [:r :g :y];[:g :o :y]  
   [:y :o]     [:g :r];[:g :r]        
   [:y :o :b]  [:r :y :b];[:r :w :g]  
   [:y :b]     [:g :y];[:g :y]        
   [:o :b]     [:o :y]})

(def two-layers-cube ;13 pieces in place
  {[:g :o] [:g :o],
   [:y :o] [:y :g],
   [:g :r] [:g :r],
   [:o :w] [:o :w],
   [:o :w :b] [:o :w :b],
   [:r :b] [:r :b],
   [:g :w :o] [:g :w :o],
   [:y :b] [:y :b],
   [:g :y] [:y :r],
   [:r :b :w] [:r :b :w],
   [:y :o :b] [:r :g :y],
   [:g :y :r] [:g :o :y],
   [:g :r :w] [:g :r :w],
   [:o :b] [:o :b],
   [:r :w] [:r :w],
   [:g :o :y] [:b :y :o],
   [:r :y] [:o :y],
   [:g :w] [:g :w],
   [:r :y :b] [:y :b :r],
   [:w :b] [:w :b]})

(def circles 
  {:b [:w :r :y :o]
   :r [:w :g :y :b]
   :w [:g :r :b :o]
   :g [:o :y :r :w]
   :y [:b :r :g :o]
   :o [:b :y :g :w]})

(def faces (keys circles))

(defn circle-next [circle color]
  (if (= (last circle) color)
    (first circle)
    (let [idx (.indexOf circle color)]
      (or (and (neg? idx) color)
        (get circle (inc idx))))))

(defn from-pos [face from-pos]
  (map 
    #(if (= face %) 
       %
       (circle-next (circles face) %))
    from-pos))

(defn find-in-cube [cube from-pos]
  (->> from-pos
       cycle
       (partition (count from-pos) 1)
       (take (count from-pos))
       (map-indexed (fn [idx p] [p (get cube (vec p)) idx]))
       (filter second)
       first))

(defn new-pos-val [cube pos face]
  ;(println cube)
  ;(println pos)
  ;(println face)
  (let [from-pos (from-pos face pos)
        [old-pos v idx] (find-in-cube cube from-pos)
        opts (->> v
                  cycle
                  (partition (count v) 1)
                  (take (count v))
                  (map vec))
        face-in-pos (.indexOf pos face)
        face-in-old-pos (.indexOf old-pos face)
        val-on-old-face (get (first opts) face-in-old-pos)]
    (first (filter
             #(= val-on-old-face (get % face-in-pos))
             opts))))

(comment
  
  (new-pos-val a-cube [:r :b :w] :b)
  [ [:o :b :y]]
  
  (new-pos-val a-cube [:o :w :b] :b)
  
  (new-pos-val a-cube [:r :y :b] :b)
  => (:r :w :g)
  )

(defn move [cube face]
  (into {}
    (map
      (fn [[pos v]]
        [pos
         (if (some #{face} pos)
           (new-pos-val cube pos face)
           v)])
      cube)))

(defn score-cube [cube]
  (let [correct-pieces
        (->> cube
             (filter (fn [[p v]] (= p v)))
             (map key)
             (map set))]
    (apply + (map
               (fn [face] (let [c (count (filter #(% face) correct-pieces))]
                            (* c c)))
               faces))))

(defn pieces-in-place [cube]
  (->> cube
       (map (fn [[p v]]
              (+
                (if (= p v)
                  1
                  0))))
       (apply +)))

(defn solved? [cube]
  (every? #(apply = %) cube))

(def ^:dynamic *depth* 6)

(defn next-moves [cube]
  {:cube cube
   :score (score-cube cube)
   :moves
   (into {}
     (for [face faces]
       [face (delay (next-moves (move cube face)))]))})

(defn beam [depth]
  (let [paths (fn [paths]
                (mapcat
                  (fn [path]
                    (map #(conj path %) faces))
                  paths))
        four-in-a-row? (fn [xs]
                         (->>
                           (partition 4 1 xs)
                           (map (partial apply =))
                           (some true?)))]
    (->> (iterate paths [[]])
         (take depth)
         last
         ;(filter (complement four-in-a-row?))
         )))

(defn search [cube beam]
  (let [mc {:cube cube 
            :score (score-cube cube)}]
    (reduce
      (fn [best-results path]
        (let [path-high (atom mc)]
          (reduce
            (fn [{:keys [cube path]} face]
              ;(println "cube " cube)
              (let [c (move cube face) ;(-> moves face deref)
                    next {:cube c
                          :score (score-cube c)}]
                ;(println "nnext" next)
                (let [r (assoc next :path (conj (or path []) face))]
                  (when (> (:score next) (:score @path-high))
                    (reset! path-high r)
                    (when (solved? (:cube next))
                      (reduced next)))
                  r)))
            mc
            path)
          (if (solved? (:cube @path-high))
            (reduced @path-high) ;;?? exit when found
            ;best-result-in-path 1; e.g. if 3 steps in was highest
            ;; if better than best-results, add to that.
            (do
              ;(println (:score path-results))
              (if (> (:score @path-high) (:score best-results))
                @path-high
                best-results)))))
      {:score 0}
      beam)))

(defn submit-callable [^Callable c ^ExecutorService ex]
  (.submit ex c))

(defn parallel-search [ex parts cube]
  ;(println (count parts))
  (let [par-results (->> parts
                         (mapv #(submit-callable (fn [] (search cube %)) ex))
                         (mapv deref))]
    ;(println par-results)
    (apply max-key :score par-results)))

(defn solve [cube depth]
  (let [beam (beam depth)
        part-count (.availableProcessors (Runtime/getRuntime))
        part-size (long (/ (count beam) part-count))
        beam-parts (partition part-size part-size [] beam)
        ex (Executors/newFixedThreadPool part-count)]
    ;(println  "part size " part-size " " (count beam-parts))
    (loop [{:keys [cube score] :as c} {:cube cube
                                       :score (score-cube cube)
                                       :path []}
           cnt 0]
      (if (or (solved? cube) (= 20 cnt))
        cube
        (let [{new-score :score path :path :as n} (parallel-search ex beam-parts cube)]
          (println (dissoc n :moves))
          (if (<= new-score score)
            (println "no progress ")
            (recur
              (update n :path conj path)
              (inc cnt))))))))



