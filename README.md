# rubik

Attempts to solve a rubik cube.

Having spent a few hours manually trying to solve a rubik cube and getting 2 of the three
layers in place but no further, I thought it'd be fair to write a program to solve it instead.

However, the search space is huge. This implementation uses the beam search method to try to work around this, 
but I have not run it for long enough (with a deep enough beam) to actually solve any cubes, although it does make progress. 

It is not using any rubik-specific algorithms to find a solution, just brute-force.
 
## Cube Encoding

The cube is a map from position -> current piece.
 
`[:g :r]` as a key means the position between the green and red faces, in that order.
  
  So a kv pair, `[:g :r] [:r :g]`  Would mean the Red/Green piece is in the right place, but is
  oriented the wrong way around.
  
  A corner kv would be like `[:g :y :r]  [:w :r :b]`, so the position is the corner between green yellow
  and red faces and the piece there currently is white-red-blue, and it's orientation is in the order
  of green-red-yellow. All corner pieces' tuples must be ordered anti-clockwise, but the starting face 
  does not matter.
 
Here is an example cube.


```
(def a-cube
  {
   [:g :r]     [:w :r]        
   [:g :y :r]  [:w :r :b ] 
   [:g :y]     [:w :b]        
   [:g :o :y]  [:w :b :o]  
   [:g :o]     [:y :r]   ;    
   [:g :w :o]  [:g :w :o]  
   [:g :w]     [:g :o]        
   [:g :r :w ] [:g :y :r]  
   [:r :w]     [:w :g]        
   [:r :b :w]  [:y :b :r]   
   [:r :y :b] [:y :o :b] ;
   [:r :b]     [:y :b]    
   [:r :y]     [:o :b]        
   [:w :b]     [:b :r]        
   [:o :w]     [:o :w]        
   [:o :w :b]  [:g :o :y]  
   [:y :o]     [:g :r]        
   [:y :o :b]  [:r :w :g]  
   [:y :b]     [:g :y]        
   [:o :b]     [:o :y]})
```

## To Solve

```
(require 'rubik.core)
(rubik.core/solve rubik.core/a-cube 9)
```

If it is not making progress at any point, it stops with a message to that effect.